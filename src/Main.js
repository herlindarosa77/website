import React, { Component } from "react";
import { Route, NavLink, HashRouter } from "react-router-dom";
import Home from "./Home";
import About from "./About";
import Contact from "./Contact";
import "./Main.css";
 
class Main extends Component {
  render() {
    return (
      <div className="">
        <HashRouter>

        {/* header */}
        <nav class="navbar navbar-expand-lg navbar-light navbar-ros">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <a class="navbar-brand" href="#">
          <img class="logo-wk" src="https://media.licdn.com/dms/image/C510BAQG1Nyx-6PqmhQ/company-logo_200_200/0?e=2159024400&v=beta&t=FhWoXyf9Ns_GDeg3sN-p2NbLyRQgAoy1_dq6aokBohw"></img>
          </a>

   
           
          <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            
            <li class="nav-item active">
                <a class="nav-link" href="#"> <span class="sr-only">(current)</span>
                <NavLink to="/home">Home</NavLink>
                </a>
              </li>

            <li class="nav-item active">
                <a class="nav-link" href="#"> <span class="sr-only">(current)</span>
                <NavLink to="/about">About</NavLink>
                </a>
              </li>

              <li class="nav-item active">
             <a class="nav-link" href="#"> <span class="sr-only">(current)</span>
             <NavLink to="/jurusan">Jurusan</NavLink>
             </a>
           </li>


            <li class="nav-item active">
             <a class="nav-link" href="#"> <span class="sr-only">(current)</span>
             <NavLink to="/contact">Contact</NavLink>
             </a>
           </li>
             
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form> 
          </div>
        </nav>



        {/* <div> */}
          <div className="content">
            <Route exact path="/" component={Home}/>
            <Route path="/about" component={About}/>
            <Route path="/contact" component={Contact}/>
          </div>
        
          <div className="footer-bottom">
                <div className="container">
                  <span>&copy; UNTUK INDONESIA </span>
                  {/* <Link to="/privacy-policy" onClick={this.topFunction}>Privacy and Policy</Link> */}
                </div>
          </div>
        {/* </div> */}

      </HashRouter>
      </div>

      
     






      
    );
  }
}
 
export default Main;