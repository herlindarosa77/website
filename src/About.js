import React, { Component } from "react";
import "./About.css"

class About extends Component {
  render() {
    return (
//       <div>
//         <h2>About</h2>
//         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
// Morbi augue tortor, laoreet et nulla et, venenatis tempor elit. 
// Nam quis nunc sapien. Integer gravida nisi eu erat semper eleifend. 
// Etiam ullamcorper metus non enim congue, et ornare nisi auctor. 
// Nullam at ullamcorper elit. Integer mattis vulputate pellentesque.</p>
        
//       </div>
<div>

{/* video */}
<center>
  <div class="jumbotron jumbotron-fluid">
        <div class="container">
          <h1 class="display-4">Fluid jumbotron</h1>
          <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
          <iframe class="video-wk" width="640" height="360" 
              src="https://www.youtube.com/embed/jPLSIYmwbWk" 
              frameborder="0" allow="accelerometer; autoplay;
              encrypted-media; gyroscope; picture-in-picture" 
              allowfullscreen>
          </iframe>
        </div>
    </div> 
 </center>

{/* content 2 */}

<div className="container">
  <div className="row">
    <div class="media">
      <img src="https://image.flaticon.com/icons/svg/1138/1138050.svg" class="align-self-center mr-3" alt="..."/>
      <div class="media-body">
        <h5 class="mt-0">Center-aligned media</h5>
        <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
        <p class="mb-0">Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
      </div>
    </div>
  </div>
</div>

{/* table */}

  <div class="jumbotron jumbotron-fluid jumbo-table">
          <div class="container">
            <h1 class="display-4">Fluid jumbotron</h1>
            <table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td colspan="2">Larry the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>
          </div>
  </div>


{/* poto kecil kecil */}
<div className="container">
      <div className="row">
      <center><h2>Apa kata mereka?</h2></center>
          <div class="card-deck">
            <div class="card">
                <img src="..." class="card-img-top" alt="..."/>
                <div class="card-body">
                  <h5 class="card-title">Card title</h5>
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                </div>
                <div class="card-footer">
                  <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>
            <div class="card">
                <img src="..." class="card-img-top" alt="..."/>
                <div class="card-body">
                  <h5 class="card-title">Card title</h5>
                  <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                </div>
                <div class="card-footer">
                  <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>
            <div class="card">
                <img src="..." class="card-img-top" alt="..."/>
                <div class="card-body">
                  <h5 class="card-title">Card title</h5>
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                </div>
                <div class="card-footer">
                  <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>
          </div>
      </div>
</div>


{/* slogan */}
<div class="jumbotron jumbotron-fluid slogan">
  <div class="container">
    <h3 class="display-5 text-center">Ilmu yg amaliah</h3>
    <p class="lead text-center">ilmu yg amaliah</p>
  
 </div>
</div>




 </div>
     
    );
  }
}
 
export default About;