import React, { Component } from "react";
import "./Contact.css"

class Contact extends Component {
  render() {
    return (
      <div className="container">
      <div class="row">
        <div class="col-6 col-4">
        <h2>Find us with maps</h2>
            <br></br>
          <div class="mapouter">
            <div class="gmap_canvas">
              <iframe width="500" height="500" 
              id="gmap_canvas" 
              src="https://maps.google.com/maps?q=smk%20wikrama%20bogor&t=&z=13&ie=UTF8&iwloc=&output=embed" 
              frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
              <a href="https://www.embedgooglemap.net/blog/best-wordpress-themes/">best wordpress themes</a>
            </div>
          </div>
        </div>
      <div class="col-6 col-4">
          <form>
            <h2>Contact Form</h2>
            <br></br>
           
          <div class="form-group">
              <label for="exampleFormControlInput1">Name</label>
              <input type="name" class="form-control" id="exampleFormControlInput1" placeholder=""/>
            </div>
            <div class="form-group">
              <label for="exampleFormControlInput1">Email address</label>
              <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"/>
            </div>
            <div class="form-group">
              <label for="exampleFormControlTextarea1">Example textarea</label>
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="6"></textarea>
            </div>
            <button type="button" class="btn btn-primary">Submit</button>
          </form>
      </div>
    </div>  
    </div>
 
    );
  }
}
 
export default Contact;