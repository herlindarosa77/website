import React from "react";
import ReactDOM from "react-dom";
import Main from "./Main";
import "./style.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
 
ReactDOM.render(
  <Main/>, 
  document.getElementById("root")
);